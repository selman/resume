# encoding: UTF-8
require 'github/markdown'
args = ARGF.argv
puts args[0].nil?
puts args[1].nil?
if args[0].nil? or args[1].nil? then
	abort("\n markdownparser [input filepath] [output filepath] \n\n")
end
if File.exist?(args[0]) then
 	input_path = args[0]
 	output_path = args[1]
 	contents = File.read(input_path)
 	doc = GitHub::Markdown.render_gfm(contents)
 	output = File.open(output_path, "w")
 	output.puts doc
 	puts contents
end
